package pl.maciejczapla;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

        String[] tablica = new String[]{
                "ala", "ola"
        };

        System.out.println(Arrays.asList(tablica));

        String[] nowaTablica = new String[tablica.length + 2];

        for (int i = 0; i < tablica.length; i++) {
            nowaTablica[i + 1] = "x" + tablica[i] + "x";
        }

        System.out.println(Arrays.asList(nowaTablica));

        String wypelnienie = "";
        for (int i = 0; i < nowaTablica[1].length(); i++) {
            wypelnienie += "x";
        }

        nowaTablica[0] = wypelnienie;
        nowaTablica[nowaTablica.length - 1] = wypelnienie;

        System.out.println(Arrays.asList(nowaTablica));

    }

}
