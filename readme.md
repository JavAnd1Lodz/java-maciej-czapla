# Java zadania


## Zadanie #1
>
> Napisać program, który pobierze od użytkownika liczbę "n" a następnie utworzy tablicę "n" liczb całkowitych i wypełni ją wartościami losowymi z przedziału [−10, . . . ,10], a następnie:
>
> * wypisze na ekranie zawartość tablicy,
> * wyznaczy najmniejszy oraz największy element w tablicy,
> * wyznaczy średnią arytmetyczną elementów tablicy,
> * wyznaczy ile elementów jest mniejszych, ile większych od średniej.
> * wypisze na ekranie zawartość tablicy w odwrotnej kolejności, tj. od ostatniego do pierwszego.


	Podaj rozmiar tablicy: 10
	Wylosowane liczby:
	-3  9  2  -10  -3  -4  -1  -5  -10  8
	Min: -10
	Max: 9
	Średnia: -1,00
	Mniejszych od średniej jest: 6
	Większych od średniej jest: 3
	Liczby w odwrotnej kolejności:
	8  -10  -5  -1  -4  -3  -10  2  9  -3



## Zadanie #2
>
> Napisz program, który stworzy tablice (macierz) "n" x "n" liczb całkowitych i wypełnij ją losowymi wartościami z zakresu {−5,−4, . . . 4, 5}, a następnie
>
> * wyświetli na ekranie wylosowaną macierz,
> * wyświetli minimalną wartość w każdym wierszu,
> * wyświetli maksymalną wartość w każdej kolumnie,
> * wyświetli maksimum dla 1. przekątnej, czyli od komórki m[0][0] do komórki m[n][n],
> * wyświetli minimum dla 2. przekątnej, czyli od komórki m[0][n] do komórki m[n][0].

	 Wylosowana macierz:
	 2	 1  -2  -5  -2
	 3	 0  -1   1  -4
	 -5  -1   3   1   4
	 -2  -2   1   1   2
	 -2  -4   4  -5   2
	 Minimalne wartości w wierszach:
	 0: -5
	 1: -4
	 2: -5
	 3: -2
	 4: -5
	 Maksymalne wartości w kolumnach:
	 0:  3
	 1:  1
	 2:  4
	 3:  1
	 4:  4
	 Maksimum na 1. przekątnej: 3
	 Minimum na 2. przekątnej: -2



## Zadanie #3
>
> Napisać program, który wczytuje od użytkownika ciąg znaków, a następnie wyświetla informację o tym ile razy w tym ciągu powtarza się jego ostatni znak.

	 Podany ciąg znaków: „abrakadabra”
	 Ostatni znak: 'a'
	 Liczba wystąpień: 5



## Zadanie #4
>
> Napisać program, który wczytuje od użytkownika ciąg znaków, a następnie sprawdza, czy podany ciąg jest palindromem (wyraz "lustrzany").
>
> Na przykład słowo `kajak` jest palindromem (to samo czytane „od przodu i od tyłu”) jednak „kotek” już nie.


## Zadanie #5
>
> Napisz program, który umożliwia szyfrowanie podanego ciągu znaków przy użyciu szyfru Cezara, który jest szczególnym przypadkiem szyfru podstawieniowego (monoalfabetycznego).
>
>
> Użytkownik programu podaje tekst do zaszyfrowania oraz liczbę "n", o którą przesunięty jest alfabet za pomocą którego szyfrujemy tekst. Dla uproszczenia można przyjąć, że łańcuch wejściowy składa się tylko z małych liter alfabetu angielskiego, tj. ’a’ – ’z’ (26 znaków) oraz spacji.

	Łańcuch znaków do zaszyfrowania: abrakadabraz
	Przesunięcie: 2
	Zaszyfrowany tekst: cdtcmcfcdtcb


## Zadanie #6
>
> Napisz funkcję, która zwraca wartość silni dla podanej liczby "n". Funkcja powinna być napisana w dwóch wersjach:
>
> * iteracyjnej i
> * rekurencyjnej.


## Zadanie #7
>
> Zdefiniować funkcję, która zwraca tablicę indeksów (pozycji) na których, w podanym łańcuchu znaków,  znajduje się znak (podany jako drugi parametr). Jeżeli znak z nie występuje w łańcuchu, to wynikiem funkcji powinna by pusta tablica.


## Zadanie #8
>
> Napisz funkcję, która zlicza:
>
> * liczbę znaków w pliku
> * liczbę białych znaków w pliku (białe znaki to spacja, tabulator, znacznik końca wiersza),
> * liczbę słów w pliku.
>
> Wynikiem funkcji jest tablica złożona z 3 liczb całkowitych po jednej dla wymienionych podpunktów.
>
>Można wykorzystać publikacje dostępne na stronie [www.wolnelektury.pl](https://wolnelektury.pl/katalog/lektury/)


## Zadanie #9
>
> Napisz funkcje, która zliczą liczbę wystąpień każdego ze słów w pliku. Wyświetl:
>
> * najpopularniejsze słowo
> * 20 najpopularniejszych słów
> * słowa, które występują więcej niż 7 a mniej niż 9 razy.
>
>Można wykorzystać publikacje dostępne na stronie [www.wolnelektury.pl](https://wolnelektury.pl/katalog/lektury/)


## Zadanie #10
>
> Napisz funkcję, która przyjmuje dwa parametry - minuty i godziny. Gdy któraś z przekazanych wartości będzie nie poprawna (czyli na przykład ujemna lub większa od 60), funkcja powinna rzucić wyjątek `IllegalArgumentException`.
>
> Dodaj obsługę rzuconego wyjątku w miejscu wykorzystania funkcji (blok `try {...} catch {...}`)


## Zadanie #11
>
> Napisz funkcje, która w pętli będzie pobierać imiona (lub dowolne inne wyrazy) aż do momentu wczytanie znaku specjalnego (który ma zostać ustalony przez nas, np. `-` lub `.`). Wykorzystaj `HashMap<>` do przechowywania podawanych informacji oraz ile razy dany wyraz został podany.
> Po zakończeniu pobierania wyświetl tylko unikalne imiona (czyli te, które wystąpiły tylko raz).


## Zadanie #12
>
> Napisz funkcje, której przekazujemy jeden element - Listę elementów typu String. Funkcja ma zwrócić informacją o liczbie unikalnych imion w przekazanej liście.
Dla Listy zawierającej `"Ola", "Ala", "Ola", "Ula"` funkcja powinna zwrócić informację `2`
