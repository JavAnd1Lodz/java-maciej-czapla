import generyczne.ParaDanych;
import przyslanianie.Kierownik;
import przyslanianie.Pracownik;
import statyczne.Informacje;
import statyczne.Manger;
import statyczne.Osoba;

public class Main {

    public static void main(String[] args) {
//        przyslanianie_1();
//        statyczne_2();
//        statyczna_osoba3();
//        anonimowa_4();
        generyczna_5();
    }

    private static void generyczna_5() {
        ParaDanych<String, Integer> para = new ParaDanych<>("Ala", 18);

        System.out.println(para.getWartoscPierwsza() + " lat " + para.getWartoscDruga());

        ParaDanych<Integer, Integer> paraDruga = new ParaDanych<>(111, 123);

    }


    private static void anonimowa_4() {
        new Pracownik() {
            @Override
            public void wyswietlObowiazki() {
                System.out.println("NADPISANO wyswietl obowiazki");
            }
        }.wyswietlObowiazki();
    }

    private static void statyczna_osoba3() {
        Osoba o1 = new Osoba();
        Osoba o2 = new Osoba();
        Osoba o3 = new Osoba();
        System.out.println("Utworzono " + Osoba.ileOsob + " osoby");

        Manger m1 = new Manger();
        System.out.println("Utworzono " + Osoba.ileOsob + " osoby");
    }

    private static void statyczne_2() {
        Informacje.wyswietlDzielnice();
        Informacje.wyswietlDzielnice();
        Informacje.ileRazyWyswietlonoDzielnice();
    }

    private static void przyslanianie_1() {
        Pracownik p1 = new Pracownik();
        p1.wyswietlnazweKlasy();
        p1.wyswietlObowiazki();

        System.out.println();
        Kierownik k1 = new Kierownik();
        k1.wyswietlObowiazki();
        k1.wyswietlnazweKlasy();
    }
}
