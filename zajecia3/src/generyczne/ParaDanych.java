package generyczne;

public class ParaDanych<PIERWSZY_TYP, DRUGI_TYP> {
    PIERWSZY_TYP wartoscPierwsza;
    DRUGI_TYP wartoscDruga;

    public ParaDanych(PIERWSZY_TYP wartoscPierwsza, DRUGI_TYP wartoscDruga) {
        this.wartoscPierwsza = wartoscPierwsza;
        this.wartoscDruga = wartoscDruga;
    }

    public PIERWSZY_TYP getWartoscPierwsza() {
        return wartoscPierwsza;
    }

    public void setWartoscPierwsza(PIERWSZY_TYP wartoscPierwsza) {
        this.wartoscPierwsza = wartoscPierwsza;
    }

    public DRUGI_TYP getWartoscDruga() {
        return wartoscDruga;
    }

    public void setWartoscDruga(DRUGI_TYP wartoscDruga) {
        this.wartoscDruga = wartoscDruga;
    }
}
