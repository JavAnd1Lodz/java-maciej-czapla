package przyslanianie;

public class Pracownik {

    public final void wyswietlnazweKlasy() {
        System.out.println("Nazwa klasy: " + this.getClass().getSimpleName());
    }

    public void wyswietlObowiazki() {
        System.out.println("Obowiązki pracownika to ...");
    }

}
