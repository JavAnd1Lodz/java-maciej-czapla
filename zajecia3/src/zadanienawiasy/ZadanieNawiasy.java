package zadanienawiasy;

public class ZadanieNawiasy {

    public static void main(String[] args) {

//        String wyraz = "Code(Cha(lle)nge)";
//        String wyraz = "The ((quick (brown) (fox) jumps over the lazy) dog)";
        String wyraz = "abcd";
        char[] tablica = wyraz.toCharArray();

        int poczatek = -1;
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] == '(') {
                poczatek = i;
            }

            if (tablica[i] == ')') {
                tablica[poczatek] = '0';
                tablica[i] = '0';

                for (int j = 1; j + poczatek < i - j; j++) {
                    char temp = tablica[poczatek + j];
                    tablica[poczatek + j] = tablica[i - j];
                    tablica[i - j] = temp;
                }

                if (poczatek != -1) {
                    poczatek = -1;
                    i = 0;
                } else {
                    break;
                }

            }
        }

        String wynik = "";
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] != '0') {
                wynik += tablica[i];
            }
        }

        System.out.println(wynik);

    }
}
