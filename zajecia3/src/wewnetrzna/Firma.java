package wewnetrzna;

public class Firma {
    public Firma(String ulica, int numer) {
        adresFirmy = new Adres();
        adresFirmy.numer = numer;
        adresFirmy.ulica = ulica;
    }

    public Adres adresFirmy;

    private class Adres {
        public String ulica;
        public int numer;
    }

}
