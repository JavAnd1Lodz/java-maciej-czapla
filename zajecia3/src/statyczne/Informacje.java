package statyczne;

public class Informacje {

    public static String nazwaMiasta = "Łodź";
    private static int ileRazyWyswietlono = 0;

    public static void wyswietlDzielnice() {
        System.out.println("Śródmieście, Polesie, Widzew...");
        ileRazyWyswietlono++;
    }

    public static void ileRazyWyswietlonoDzielnice() {
        System.out.println("Dzielnice wyswietlono " + ileRazyWyswietlono + " razy");
    }

}
