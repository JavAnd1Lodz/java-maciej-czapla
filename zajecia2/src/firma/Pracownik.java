package firma;

public class Pracownik {
    String imie;
    int pensja;

    public Pracownik(String imie) {
        this.imie = imie;
    }

    public Pracownik(String imie, int pensja) {
        this.imie = imie;
        this.pensja = pensja;
    }

    public void ustawPensje(int pensja) {
        this.pensja = pensja;
    }
}
