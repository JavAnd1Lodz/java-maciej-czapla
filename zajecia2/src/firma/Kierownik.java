package firma;

public class Kierownik extends Pracownik {
    int bonus;

    public Kierownik(String imie, int pensja, int bonus) {
        super(imie, pensja);
        this.bonus = bonus;
    }
}
