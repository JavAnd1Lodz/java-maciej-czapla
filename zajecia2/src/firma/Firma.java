package firma;

import java.util.ArrayList;
import java.util.List;

public class Firma {
    private String nazwaFirmy;
    private List<Pracownik> listaPracownikow = new ArrayList<>();

    public Firma(String nazwaFirmy) {
        this.nazwaFirmy = nazwaFirmy;
    }

    public void dodajPracownika(Pracownik pracownik) {
        listaPracownikow.add(pracownik);
    }

    public void dodajKilkuPracownikow(Pracownik... pracownicy) {
        /** imiona = [Adam, Ewa, Ola]
         * pracownicy[0]  <--  wyświetli mi "Adam"
         * pracownicy[1]  <--  wyświetli mi "Ewa"
         * pracownicy[2]  <--  wyświetli mi "Ola"
         */

        // petla po tablicy przyjętej jako parametr
        for (int i = 0; i < pracownicy.length; i++) {
            listaPracownikow.add(pracownicy[i]);
        }
//         równoznaczne:
//        listaPracownikow.addAll(Arrays.asList(pracownicy));
    }

    public void wyswietlWszystkichPracownikow(boolean czyWyswietlicPensje) {

//        for (int i = 0; i < listaPracownikow.size(); i++) {
//
//        }

        String wiadomosc;
        // pętla foreach
        for (Pracownik pracownik : listaPracownikow) {
            // dodaje imie
            wiadomosc = pracownik.imie;

            // dodaje pensję
            if (czyWyswietlicPensje) {
                if (pracownik instanceof Kierownik) {
                    Kierownik kierownik = (Kierownik) pracownik;
                    wiadomosc += String.format(" (%s zł + %s zł bonus)",
                            pracownik.pensja, kierownik.bonus);
                } else {
                    wiadomosc += String.format(" (%s zł)", pracownik.pensja);
                }
            }
            // wyświetlam
            System.out.println(wiadomosc);
        }
    }

    public int miesiecznyKoszt() {
        int koszt = 0;
        for (int i = 0; i < listaPracownikow.size(); i++) {
            Pracownik pracownik = listaPracownikow.get(i);
            koszt += pracownik.pensja;
            if (pracownik instanceof Kierownik) {
                Kierownik kierownik = (Kierownik) pracownik;
                koszt += kierownik.bonus;
            }
        }
        return koszt;
    }
}
