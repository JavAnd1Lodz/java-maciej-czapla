package modulo;

public class SubString {

    public static void main(String[] arg) {

        String tekst = "Aa12b3CCb457B8aaaaa";
        String[] tablica = new String[tekst.length()];

        String podciag = "";
        int kolejny = 0;
        for (int i = 0; i < tekst.length(); i++) {
            char znak = tekst.charAt(i);
            if (Character.isDigit(znak)) {
                // jest cyfrą
                if (!podciag.isEmpty()) {
                    tablica[kolejny] = podciag;
                    kolejny++;
                }
                podciag = "";
            } else {
                // nie jest cyfrą
                podciag += znak;
            }

            if (i == tekst.length() - 1) {
                if (!podciag.isEmpty()) {
                    tablica[kolejny] = podciag;
                    kolejny++;
                }
            }
        }

        int max = 0;
        for (int j = 0; j < tablica.length; j++) {
            if (tablica[j] != null) {
                if (tablica[j] != tablica[j].toLowerCase()) {
                    System.out.println(tablica[j]);
                    if (tablica[j].length() > max) {
                        max = tablica[j].length();
                    }
                }
            }
        }
        System.out.println(max);
//        System.out.println(Arrays.toString(tablica));


    }

}
