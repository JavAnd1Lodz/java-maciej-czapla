package modulo;

public class Modulo {

    public static void main(String[] args) {
        int liczba = 110;

        String wiadomosc = "";
        for (int i = 1; i < liczba+1; i++) {
            wiadomosc = String.valueOf(i);

            if (i % 3 == 0){
                wiadomosc += String.valueOf("A");
            }
            if (i % 5 == 0){
                wiadomosc += String.valueOf("B");
            }
            if (i % 7 == 0){
                wiadomosc += String.valueOf("C");
            }


            System.out.println(wiadomosc);
        }

    }
}
