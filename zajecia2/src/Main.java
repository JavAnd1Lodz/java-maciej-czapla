import budynki.Budynki;
import dodawanie.Dodawanie;
import firma.Firma;
import firma.Kierownik;
import firma.Pracownik;
import punkt.Punkt;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {

//        dodawanie_1();
//        punkt_2();
//        firma_3();
        budynki_4();
    }

    private static void budynki_4() {
        int[] tablica = new int[]{1, 2, 0, 3, 2, 1, 3};
        Budynki b = new Budynki(tablica);
        b.setMaxWysokosc(b.policzMaxWysokosc());

        int liczbaKlockow = 0;
        for (int i = 0; i < b.getMaxWysokosc(); i++) {
            System.out.println(Arrays.toString(tablica));
            liczbaKlockow += b.analizujWiersz();
        }
        System.out.println("Liczba klocków poziomych: " + liczbaKlockow);

    }


    private static void firma_3() {
        Firma f = new Firma("Moja firma krzak");

        Pracownik p1 = new Pracownik("Anna Nowak", 3456);
        Pracownik p2 = new Pracownik("Piotr Kowalski", 2374);

        Pracownik p3 = new Pracownik("Jan Nowak");
        p3.ustawPensje(2000);

        Pracownik p4 = new Kierownik("Adam Król", 10000, 2000);

        f.dodajPracownika(p1);
        f.dodajKilkuPracownikow(p2, p3);
        // dodaje kierownika:
        f.dodajPracownika(p4);

        // parametr odpowiada za wyswietlenie pensji
        // true = wyświetla imie + pensje
        // false = wyswietla tylko imie
        f.wyswietlWszystkichPracownikow(true);

        int koszt = f.miesiecznyKoszt(); // sumujacy pensje za jeden miesiac
        System.out.println(koszt);

    }


    private static void punkt_2() {
        Punkt p1 = new Punkt();
        Punkt p2 = new Punkt(4, 2);
        Punkt p3 = new Punkt(new Punkt(2, 7));

    }

    private static void dodawanie_1() {
        Dodawanie d = new Dodawanie();
        d.dodaj(1, 3);
        d.dodaj(1, 3, 7);
        d.dodaj(1f, 2, 1);
    }
}
