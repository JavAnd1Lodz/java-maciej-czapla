package dodawanie;

public class Dodawanie {

    public int dodaj(int a, int b) {
        return a + b;
    }

    public int dodaj(int a, int b, int c) {
        return a + b + c;
    }

    public float dodaj(float... parametry) {
        int suma = 0;
        for (int i = 0; i < parametry.length; i++) {
            suma += parametry[i];
        }
        return suma;
    }

}
