package budynki;

public class Budynki {

    int[] tablicaWejsciowa;
    int maxWysokosc;

    public int getMaxWysokosc() {
        return maxWysokosc;
    }

    public void setMaxWysokosc(int maxWysokosc) {
        this.maxWysokosc = maxWysokosc;
    }

    public Budynki(int[] tablica) {
        this.tablicaWejsciowa = tablica;
    }

    public int policzMaxWysokosc() {
        int max = 0;
        for (int i = 0; i < tablicaWejsciowa.length; i++) {
            if (tablicaWejsciowa[i] > max) {
                max = tablicaWejsciowa[i];
            }
        }
        return max;
    }

    public int analizujWiersz() {
        int licznik = 0;
        int bloki = 0;

        for (int i = 0; i < tablicaWejsciowa.length; i++) {
            int element = tablicaWejsciowa[i];
            if (element == 0) {
                if (licznik != 0) {
                    bloki++;
                }
                licznik = 0;
            } else {
                tablicaWejsciowa[i]--;
                licznik++;
            }
            System.out.print(licznik + ", ");
            if (i == tablicaWejsciowa.length - 1) {
                System.out.println("koniec");
                if (licznik != 0) {
                    bloki++;
                }
            }
        }
        return bloki;
    }
}
