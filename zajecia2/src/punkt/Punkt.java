package punkt;

public class Punkt {
    int x, y;

    public Punkt() {
        // domyslne wartosci
        y = 0;
        x = 0;
    }

    public Punkt(Punkt punkt) {
        x = punkt.x;
        y = punkt.y;
    }

    public Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }

}
