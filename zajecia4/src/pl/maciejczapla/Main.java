package pl.maciejczapla;

import pl.maciejczapla.konto.Konto;
import pl.maciejczapla.watki.podstawy.MojWlasnyWatek;
import pl.maciejczapla.watki.podstawy.MojaKlasa;

public class Main {

    public static void main(String[] args) {
//        podstawy_1();
//        podstawy_2();
//        podstawy_runnable();
        konto();
    }

    private static void konto() {
        final int ileRazy = 10000;
        final int kwota = 10000;
        final Konto konto = new Konto(1, "Maciej", 1000);

        Thread wplacanie = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < ileRazy; i++) {
                    konto.wplata(kwota);
                }
            }
        });

        Thread wyplacanie = new Thread(new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i < ileRazy; i++) {
                    konto.wyplata(kwota);
                }
            }
        });

        wplacanie.start();
        wyplacanie.start();

        try {
            wplacanie.join();
            wyplacanie.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Kwota na końcu: " + konto.getSaldo());

    }

    private static void podstawy_runnable() {
        System.out.println("Obecny wątek ma ID: " + Thread.currentThread().getId());
        MojaKlasa mk = new MojaKlasa();
        Thread nowyWatek = new Thread(mk);
        nowyWatek.start();
    }


    private static void podstawy_2() {
        MojWlasnyWatek pierwszy = new MojWlasnyWatek(10);
        MojWlasnyWatek drugi = new MojWlasnyWatek(20);

        pierwszy.start();
        drugi.start();

        try {
            pierwszy.join();
//            drugi.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Koniec maina");
    }

    private static void podstawy_1() {
        long identyfikator = Thread.currentThread().getId();
        System.out.println("ID obecnego wątku: " + identyfikator);

        MojWlasnyWatek mojWlasnyWatek = new MojWlasnyWatek(10);
        mojWlasnyWatek.start();

        for (int i = 0; i < 100; i++) {
            try {
                System.out.println(String.format("Wątek %s, obrót nr: %s", identyfikator, i));
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }
}
