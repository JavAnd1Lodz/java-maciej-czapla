package pl.maciejczapla.konto;

public class Konto {
    int numerKonta;
    String wlasciciel;
    long saldo;

    public Konto(int numerKonta, String wlasciciel, long saldo) {
        this.numerKonta = numerKonta;
        this.wlasciciel = wlasciciel;
        this.saldo = saldo;
    }

    public synchronized void wplata(long kwota) {
        if (kwota <= 0) {
            throw new IllegalArgumentException("Błędna kwota!");
        }
        saldo += kwota;
    }

    public synchronized void wyplata(long kwota) {
        if (kwota <= 0) {
            throw new IllegalArgumentException("Błędna kwota!");
        }

        if (saldo < kwota) {
            throw new IllegalArgumentException("Brak środków na koncie nr: " + numerKonta);
        }

        saldo -= kwota;
    }


    public int getNumerKonta() {
        return numerKonta;
    }

    public void setNumerKonta(int numerKonta) {
        this.numerKonta = numerKonta;
    }

    public String getWlasciciel() {
        return wlasciciel;
    }

    public void setWlasciciel(String wlasciciel) {
        this.wlasciciel = wlasciciel;
    }

    public long getSaldo() {
        return saldo;
    }

    public void setSaldo(long saldo) {
        this.saldo = saldo;
    }
}
