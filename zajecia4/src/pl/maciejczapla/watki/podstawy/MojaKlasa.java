package pl.maciejczapla.watki.podstawy;

public class MojaKlasa implements Runnable {
    @Override
    public void run() {
        System.out.println("Uruchomiono nowy wątek o ID: " + Thread.currentThread().getId());
    }
}
