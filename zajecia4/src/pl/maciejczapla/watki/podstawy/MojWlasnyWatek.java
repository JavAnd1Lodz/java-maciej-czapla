package pl.maciejczapla.watki.podstawy;

public class MojWlasnyWatek extends Thread {
    int liczba;

    public MojWlasnyWatek(int i) {
        this.liczba = i;
    }

    @Override
    public void run() {

        long identyfikator = Thread.currentThread().getId();
        for (int i = 0; i < liczba; i++) {
            try {
                System.out.println(String.format("Wątek %s, obrót nr: %s", identyfikator, i));
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
