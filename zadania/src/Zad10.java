import java.io.File;
import java.io.FileNotFoundException;
import java.text.Collator;
import java.util.*;

/**
 * Napisz funkcję, która przyjmuje dwa parametry - minuty i godziny.
 * Gdy któraś z przekazanych wartości będzie nie poprawna
 * (czyli na przykład ujemna lub większa od 60),
 * funkcja powinna rzucić wyjątek IllegalArgumentException.
 * Dodaj obsługę rzuconego wyjątku w miejscu wykorzystania funkcji (blok try {...} catch {...})
 */

public class Zad10 {

    private static void sprawdzCzas(int godziny, int minuty) throws IllegalAccessException {
        if (godziny < 0 || godziny > 23) {
            throw new IllegalAccessException("Błednie podana godzina");
        }
        if (minuty < 0 || minuty >= 60) {
            throw new IllegalAccessException("Błednie podane minuty");
        }
        System.out.println(String.format("%s:%s", godziny, minuty));
    }

    public static void main(String[] argumenty) {
        try {
            sprawdzCzas(23, 60);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

    }
}
