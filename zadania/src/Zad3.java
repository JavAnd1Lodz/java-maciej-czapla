import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Program, który wczytuje od użytkownika ciąg znaków,
 * a następnie wyświetla informację o tym ile razy w tym ciągu
 * powtarza się jego ostatni znak.
 */

public class Zad3 {

    public static void main(String[] argumenty) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Podaj ciąg znaków: ");
            // ODCZYTANIE CIAGU ZNAKOW Z KONSOLI
            String ciagZnakow = br.readLine();
            System.out.println("Wczytany ciąg znaków: " + ciagZnakow);

            // OSTATNI ZNAK
            char ostatniZnak = ciagZnakow.charAt(ciagZnakow.length() - 1);
            System.out.println("Ostatni znak: " + ostatniZnak);

            // LICZENIE WYSTAPIEN
            int liczbaWystapien = 0;
            StringBuilder zaznaczenieWystpien = new StringBuilder();
            for (int i = 0; i < ciagZnakow.length(); i++) {
                char pojedynczyZnak = ciagZnakow.charAt(i);
                if (pojedynczyZnak == ostatniZnak) {
                    liczbaWystapien++;
                    zaznaczenieWystpien.append("^");
                } else {
                    zaznaczenieWystpien.append(" ");
                }
            }
            System.out.println(String.format("Znak '%s' wystepuje: %s razy", ostatniZnak, liczbaWystapien));
            System.out.println();
            System.out.println(ciagZnakow);
            System.out.println(zaznaczenieWystpien);


        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
