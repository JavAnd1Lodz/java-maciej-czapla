import java.util.*;

/**
 * Napisz funkcje, której przekazujemy jeden element - Listę elementów typu String.
 * Funkcja ma zwrócić informacją o liczbie unikalnych imion w przekazanej liście.
 * Dla Listy zawierającej "Ola", "Ala", "Ola", "Ula" funkcja powinna zwrócić informację 2
 */

public class Zad12 {

    static int liczbaUnikalnychElementow(List<String> wszystkieElementy) {
        int unikalne = 0;
        final Set<String> set1 = new HashSet<>();

        for (String element : wszystkieElementy) {
            if (!set1.add(element)) {
                unikalne++;
            } else {
                unikalne--;
            }
        }
        return unikalne;
    }

    public static void main(String[] argumenty) {

        List<String> wszystkieElementy = new ArrayList<>();
        Collections.addAll(wszystkieElementy,
                "Ola", "Ala", "Ola", "Ula");

        int unikalne = liczbaUnikalnychElementow(wszystkieElementy);

        System.out.println("Liczba unikalnych pozycji: " + unikalne);

    }
}
