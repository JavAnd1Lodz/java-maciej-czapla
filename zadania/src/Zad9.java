import java.io.*;
import java.text.Collator;
import java.util.*;

/**
 * Napisz funkcje, która zliczą liczbę wystąpień każdego ze słów w pliku.
 * Wyświetl:
 * - najpopularniejsze słowo
 * - 20 najpopularniejszych słów
 * - słowa, które występują więcej niż 7 a mniej niż 9 razy.
 */

public class Zad9 {

    private static TreeMap<String, Integer> znajdzWystapienia() {
        TreeMap<String, Integer> mapaWystapien = new TreeMap<>(Collator.getInstance(new Locale("pl", "PL")));
        try {
            File file = new File("src/pliki_tekstowe/zad9-pan-tadeusz.txt");
            Scanner scanner = new Scanner(file);
            scanner.useDelimiter("[^\\p{L}]+");
            while (scanner.hasNext()) {
                String slowo = scanner.next();
                if (mapaWystapien.containsKey(slowo)) {
                    int ileBylo = mapaWystapien.get(slowo);
                    mapaWystapien.put(slowo, ileBylo + 1);
                } else {
                    mapaWystapien.put(slowo, 1);
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return mapaWystapien;
    }

    static void najpopularniejszeSlowo(List<Map.Entry<String, Integer>> mapaWystapien) {
        Map.Entry<String, Integer> najpopularniejszy = mapaWystapien.get(mapaWystapien.size() - 1);
        System.out.println(String.format("Najpopularniejsze słowo: %s (liczba wystapień: %s)",
                najpopularniejszy.getKey(), najpopularniejszy.getValue()));
    }

    static void najpopularniejszeDwadziesciaSlow(List<Map.Entry<String, Integer>> mapaWystapien) {
        int max = 20;
        System.out.println(String.format("Najpopularniejsze %s słów: ", max));
        for (int i = mapaWystapien.size() - max; i < mapaWystapien.size(); i++) {
            Map.Entry<String, Integer> element = mapaWystapien.get(i);
            System.out.print(String.format("%s (%s razy), ", element.getKey(), element.getValue()));
        }
        System.out.println();
    }

    static void liczbaWystapien(List<Map.Entry<String, Integer>> mapaWystapien, int min, int max) {
        System.out.println(String.format("Słowa które występują od %s do %s razy:", min, max));
        for (int i = 0; i < mapaWystapien.size(); i++) {
            if (mapaWystapien.get(i).getValue() >= min
                    && mapaWystapien.get(i).getValue() <= min)
                System.out.print(mapaWystapien.get(i).getKey() + ", ");
        }
        System.out.println();
    }

    public static void main(String[] argumenty) {
        // POSORTOWANA ALFABETYCZNIE
        TreeMap<String, Integer> mapaWystapien = znajdzWystapienia();

        // BĘDZIE POSORTOWANA PO LICZBIE WYSTAPIEN
        List<Map.Entry<String, Integer>> lista = new ArrayList<>(mapaWystapien.entrySet());

        // SORTOWANIE PO LICZBIE WYSTAPIEN
        Collections.sort(lista, new Comparator<Map.Entry<String, Integer>>() {
            @Override
            public int compare(Map.Entry<String, Integer> o1, Map.Entry<String, Integer> o2) {
                return o1.getValue() - o2.getValue();
            }
        });

        najpopularniejszeSlowo(lista);
        System.out.println("=============================================");
        najpopularniejszeDwadziesciaSlow(lista);
        System.out.println("=============================================");
        liczbaWystapien(lista, 7, 9);

    }
}
