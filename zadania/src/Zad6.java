import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Napisz funkcję, która zwraca wartość silni dla podanej liczby "n".
 * Funkcja powinna być napisana w dwóch wersjach:
 * - iteracyjnej i
 * - rekurencyjnej.
 */

public class Zad6 {

    private static long silniaIteracyjnie(long liczba) {
        long wynik = 1;
        for (long i = 1; i <= liczba; i++) {
            wynik *= i;
        }
        return wynik;
    }


    private static long silniaRekurencyjnie(long liczba) {
        if (liczba == 1) {
            return 1;
        }
        return liczba * (silniaRekurencyjnie(liczba - 1));
    }

    public static void main(String[] argumenty) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Podaj liczbe: ");
            // ODCZYTANIE LICZBY Z KONSOLI
            long liczba = Long.parseLong(br.readLine());

            // LICZENIE SILNI
            System.out.println("Silnia iteracyjnie: " + silniaIteracyjnie(liczba));
            System.out.println("Silnia rekurencyjnie: " + silniaRekurencyjnie(liczba));

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
