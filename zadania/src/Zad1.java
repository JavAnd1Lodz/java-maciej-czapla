import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Napisać program, który pobierze od użytkownika liczbę "n"
 * a następnie utworzy tablicę "n" liczb całkowitych
 * i wypełni ją wartościami losowymi z przedziału [−10, . . . ,10],
 * a następnie:
 * - wypisze na ekranie zawartość tablicy,
 * - wyznaczy najmniejszy oraz największy element w tablicy,
 * - wyznaczy średnią arytmetyczną elementów tablicy,
 * - wyznaczy ile elementów jest mniejszych, ile większych od średniej.
 * - wypisze na ekranie zawartość tablicy w odwrotnej kolejności, tj. od ostatniego do pierwszego.
 */

public class Zad1 {

    public static void main(String[] argumenty) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Podaj rozmiar tablicy: ");
            // ODCZYTANIE ROZMIARU Z KONSOLI
            int rozmiar = Integer.valueOf(br.readLine());

            // BLEDNY ROZMIAR
            if (rozmiar < 1) {
                return;
            }

            // UTWORZENIE TABLICY
            int[] tablica = new int[rozmiar];

            Random random = new Random();

            // LOSOWANIE
            for (int i = 0; i < rozmiar; i++) {
                tablica[i] = random.nextInt(21) - 10;
            }

            // WYSWIETLANIE
            for (int i = 0; i < tablica.length; i++) {
                System.out.print(tablica[i] + "   ");
            }
            System.out.println();

            // NAJMNIEJSZY I NAJWIEKSZY ELEMENT
            int min = tablica[0];
            int max = tablica[0];

            for (int i = 0; i < rozmiar; i++) {
                if (tablica[i] < min) {
                    // ZNALEZIONONO MNIEJSZY
                    min = tablica[i];
                }
                if (tablica[i] > max) {
                    // ZNALEZIONO WIEKSZY
                    max = tablica[i];
                }
            }

            System.out.println("Najmniejszy element: " + min);
            System.out.println("Najwiejszy element: " + max);

            // POLICZENIE SUMY ELEMENTÓW
            float srednia = 0;

            for (int i = 0; i < rozmiar; i++) {
                srednia += tablica[i];
            }

            // POLICZENIE
            srednia /= rozmiar;

            System.out.println("Średnia elementów w tablicy: " + srednia);

            // ILE MNIEJSZYCH I ILE WIEKSZYCH
            int ileMniejszych = 0;
            int ileWiekszych = 0;
            for (int i = 0; i < rozmiar; i++) {
                if(tablica[i] < srednia) {
                    ileMniejszych++;
                } else {
                    ileWiekszych++;
                }
            }

            System.out.println("Elementów mniejszych od sredniej: " + ileMniejszych);
            System.out.println("Elementów wiekszych od sredniej: " + ileWiekszych);

            // WYSWIETLENIE W ODWROTNEJ KOLEJNOSCI
            System.out.println("W odwrotnej kolejnoci: " + ileWiekszych);
            for (int i = 0; i < tablica.length; i++) {
                System.out.print(tablica[rozmiar-i-1] + "   ");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
