import java.io.*;
import java.util.Scanner;

/**
 * Napisz funkcję, która zlicza:
 * - liczbę znaków w pliku
 * - liczbę białych znaków w pliku (białe znaki to spacja, tabulator, znacznik końca wiersza),
 * - liczbę słów w pliku.
 * Wynikiem funkcji jest tablica złożona z 3 liczb całkowitych
 * po jednej dla wymienionych podpunktów.
 * Można wykorzystać publikacje dostępne na stronie www.wolnelektury.pl
 */

public class Zad8 {

    private static int[] znajdzWystapienia() {
        int[] tablicaZWynikami = new int[3];
        // [0] znaki w pliku
        // [1] biale znaki w pliku
        // [2] słowa w pliku

        try {
            File file = new File("src/pliki_tekstowe/zad8.txt");
            FileReader fileReader = new FileReader(file);
            BufferedReader reader = new BufferedReader(fileReader);

            // LICZENIE ZNAKOW I BIAŁYCH ZNAKÓW
            int znak = reader.read();
            while (znak != -1) {
                if (Character.isWhitespace(znak)) {
                    tablicaZWynikami[1] += 1;
                } else {
                    tablicaZWynikami[0] += 1;
                }
                znak = reader.read();
            }

            // LICZENIE SLOW
            FileInputStream fileInputStream = new FileInputStream(file);
            Scanner sc = new Scanner(fileInputStream);
            while (sc.hasNext()) {
                sc.next();
                tablicaZWynikami[2] += 1;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return tablicaZWynikami;
    }

    public static void main(String[] argumenty) {
        int[] wynik = znajdzWystapienia();

        System.out.println("Liczba znaków w pliku: " + wynik[0]);
        System.out.println("Liczba białych znaków w pliku: " + wynik[1]);
        System.out.println("Liczba słów w pliku: " + wynik[2]);

    }
}
