import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Napisz program, który umożliwia szyfrowanie podanego ciągu znaków
 * przy użyciu szyfru Cezara, który jest szczególnym przypadkiem
 * szyfru podstawieniowego (monoalfabetycznego).
 * Użytkownik programu podaje tekst do zaszyfrowania oraz liczbę "n",
 * o którą przesunięty jest alfabet za pomocą którego szyfrujemy tekst.
 * Dla uproszczenia można przyjąć, że łańcuch wejściowy składa się tylko
 * z małych liter alfabetu angielskiego, tj. ’a’ – ’z’ (26 znaków) oraz spacji.
 */

public class Zad5 {

    public static void main(String[] argumenty) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Podaj ciąg znaków: ");
            // ODCZYTANIE CIAGU ZNAKOW Z KONSOLI
            String tekstDoZaszyfrowania = br.readLine();

            System.out.print("Podaj przesunięcie: ");
            // ODCZYTANIE CIAGU ZNAKOW Z KONSOLI
            int przesunięcie = Integer.parseInt(br.readLine());

            System.out.println("Wczytany wyraz: " + tekstDoZaszyfrowania);
            System.out.println("Należy przesunąć znaki o: " + przesunięcie);

            // SZYFR CEZARA
            String zaszyfrowany = szyfrCezara(tekstDoZaszyfrowania, przesunięcie);

            System.out.println("Tekst po zaszyfrowaniu: " + zaszyfrowany);

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static String szyfrCezara(String tekst, int przesunięcie) {
        StringBuilder tekstWynikowy = new StringBuilder();

        for (int i = 0; i < tekst.length(); i++) {
            char pojedynczyZNak = (char) (tekst.charAt(i) + przesunięcie);
            if (pojedynczyZNak > 'z') {
                // SKRAJNY PRZYPADEK, TJ. KONIEC ALFABETU
                tekstWynikowy.append((char) (tekst.charAt(i) - (26 - przesunięcie)));
            } else {
                tekstWynikowy.append((char) (tekst.charAt(i) + przesunięcie));
            }
        }

        return tekstWynikowy.toString();
    }

}
