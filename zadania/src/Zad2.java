import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;

/**
 * Napisz program, który stworzy tablice (macierz) "n" x "n" liczb całkowitych
 * i wypełnij ją losowymi wartościami z zakresu {−5,−4, . . . 4, 5}, a następnie wyświetli
 * - wylosowaną macierz,
 * - minimalną wartość w każdym wierszu,
 * - maksymalną wartość w każdej kolumnie,
 * - maksimum dla 1. przekątnej, czyli od komórkim[0][0] do komórkim[n][n],
 * - minimum dla 2. przekątnej, czyli od komórkim[0][n]do komórkim[n][0].
 */

public class Zad2 {

    public static void main(String[] argumenty) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Podaj rozmiar tablicy: ");
            // ODCZYTANIE ROZMIARU Z KONSOLI
            int rozmiar = Integer.valueOf(br.readLine());
            // UTWORZENIE TABLICY DWUWYMIAROWEJ
            int[][] tablica = new int[rozmiar][rozmiar];

            Random random = new Random();

            // LOSOWANIE
            for (int i = 0; i < rozmiar; i++) {
                for (int j = 0; j < rozmiar; j++) {
                    tablica[i][j] = random.nextInt(21) - 10;
                }
            }


            // WYSWIETLANIE
            System.out.println("Wylosowana macierz:");
            // PETLA PO WIERSZACH
            for (int i = 0; i < tablica.length; i++) {
                // PETLA PO KOLUMNACH
                for (int j = 0; j < tablica.length; j++) {
                    // WYSWIETLENIE + ZAPEWNIENIE 4 ZNAKÓW
                    System.out.printf("%4d", tablica[i][j]);
                }
                // ZŁAMANIE WIERSZA
                System.out.println();
            }


            // NAJMNIEJSZY ELEMENT W KAŻDYM WIERSZU
            System.out.println("\nMinimalne wartości w wierszach:");
            for (int i = 0; i < rozmiar; i++) {
                int min = tablica[i][0];
                for (int j = 0; j < rozmiar; j++) {
                    if (tablica[i][j] < min) {
                        // ZNALEZIONONO MNIEJSZY
                        min = tablica[i][j];
                    }
                }
                System.out.println(String.format("%s: %s", i, min));
            }


            // NAJWIĘKSZY ELEMENT W KAŻDEJ KOLUMNIE
            System.out.println("\nMaksymalne wartości w kolumnach:");
            for (int i = 0; i < rozmiar; i++) {
                int max = tablica[0][i];
                for (int j = 0; j < rozmiar; j++) {
                    if (tablica[j][i] > max) {
                        // ZNALEZIONONO MNIEJSZY
                        max = tablica[j][i];
                    }
                }
                System.out.println(String.format("%s: %s", i, max));
            }

            // MAKSIMUM DLA 1. PRZEKĄTNEJ, CZYLI OD KOMÓRKI M[0][0] DO KOMÓRKI M[N][N]
            int max = tablica[0][0];
            for (int i = 0; i < rozmiar; i++) {
                for (int j = 0; j < rozmiar; j++) {
                    if (i == j) {
                        if (tablica[i][j] > max) {
                            // ZNALEZIONO WIEKSZY
                            max = tablica[i][j];
                        }
                    }
                }
            }
            System.out.println("\nMaksimum na 1. przekątnej: " + max);


            // MINIMUM DLA 2. PRZEKĄTNEJ, CZYLI OD KOMÓRKI M[0][N] DO KOMÓRKI M[N][0]
            int min = tablica[0][0];
            for (int i = 0; i < rozmiar; i++) {
                for (int j = 0; j < rozmiar; j++) {
                    if (j == rozmiar-i-1) {
                        if (tablica[i][j] < min) {
                            // ZNALEZIONO MNIEJSZY
                            min = tablica[i][j];
                        }
                    }
                }
            }
            System.out.println("\nMinimum na 2. przekątnej: " + min);



        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
