import java.util.*;

/**
 * Napisz funkcje, która w pętli będzie pobierać imiona (lub dowolne inne wyrazy)
 * aż do momentu wczytanie znaku specjalnego
 * (który ma zostać ustalony przez nas, np. - lub .).
 * Wykorzystaj HashSet<> do przechowywania podawanych informacji
 * oraz ile razy dany wyraz został podany.
 * Po zakończeniu pobierania wyświetl tylko unikalne imiona
 * (czyli te, które wystąpiły tylko raz).
 */

public class Zad11 {

    public static void main(String[] argumenty) {
        HashMap<String, Integer> wystapienia = new HashMap<String, Integer>();
        Scanner scanner = new Scanner(System.in);
        System.out.print("Podaj wyraz: ");
        // ODCZYTANIE CIAGU ZNAKOW Z KONSOLI
        String znakKonca = "=";
        String slowo;

        while (scanner.hasNextLine()) {
            slowo = scanner.next().trim();

            if (slowo.equals(znakKonca)) {
                break;
            }

            if (wystapienia.containsKey(slowo)) {
                int ileBylo = wystapienia.get(slowo);
                wystapienia.put(slowo, ileBylo + 1);
            } else {
                wystapienia.put(slowo, 1);
            }
        }

        System.out.println(wystapienia);

        System.out.println("Wyrazy które zostały podane tylko raz: ");
        for (Map.Entry<String, Integer> element : wystapienia.entrySet()) {
            if (element.getValue() == 1) {
                System.out.print(element.getKey() + ", ");
            }

        }
    }
}
