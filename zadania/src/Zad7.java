import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

/**
 * Zdefiniować funkcję, która zwraca indeks (pozycję) na którym znajduje
 * się znak (drugi parametr) w podanym łańcuchu znaków.
 * Jeżeli znak z nie występuje w łańcuchu, to wynikiem funkcji
 * powinna być pusta tablica. Uwaga - pozycje znaków numerujemy począwszy od 0.
 */

public class Zad7 {

    private static int[] znajdzWystapienia(String lancuchZnakow, char znak) {
        int[] pozycje = new int[0];
        for (int i = 0; i < lancuchZnakow.length(); i++) {
            if (lancuchZnakow.charAt(i) == znak) {
                pozycje = Arrays.copyOf(pozycje, pozycje.length + 1);
                pozycje[pozycje.length - 1] = i;
            }
        }

        return pozycje;
    }

    public static void main(String[] argumenty) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            // ODCZYTANIE LICZBY Z KONSOLI
            System.out.print("Podaj łańcuch znaków: ");
            String lancuchZnakow = br.readLine();

            // ODCZYTANIE POSZUKIWANEGO ZNAKU
            System.out.print("Podaj poszukiwany znak: ");
            char poszukiwanyZnak = (char) br.read();

            System.out.println(Arrays.toString(znajdzWystapienia(lancuchZnakow, poszukiwanyZnak)));


        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
