import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Napisać program, który wczytuje od użytkownika ciąg znaków,
 * a następnie sprawdza, czy podany ciąg jest palindromem (wyraz "lustrzany").
 * Na przykład słowo kajak jest palindromem (to samo czytane „od przodu i od tyłu”)
 * jednak słowo „kotek” już nie.
 */

public class Zad4 {

    private static boolean czyPalindrom(String wyraz) {
        boolean czyPalindrom = true;
        int rozmiarTablicy = wyraz.length();
        for (int i = 0; i < (rozmiarTablicy / 2) + 1; i++) {
            if (wyraz.charAt(i) != wyraz.charAt(rozmiarTablicy - i - 1)) {
                czyPalindrom = false;
                break;
            }
        }
        return czyPalindrom;
    }

    public static void main(String[] argumenty) {
        try {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Podaj ciąg znaków: ");
            // ODCZYTANIE CIAGU ZNAKOW Z KONSOLI
            String wyrazDoSprawdzenia = br.readLine();
            System.out.println("Wczytany wyraz: " + wyrazDoSprawdzenia);

            boolean czyPalindrom = czyPalindrom(wyrazDoSprawdzenia);

            System.out.println(String.format("Wyraz '%s' %s palindromem.",
                    wyrazDoSprawdzenia,
                    czyPalindrom ? "jest" : "nie jest"));

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
