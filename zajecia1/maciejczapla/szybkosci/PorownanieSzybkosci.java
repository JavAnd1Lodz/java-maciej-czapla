package szybkosci;

import java.util.List;

public class PorownanieSzybkosci {

    public void testujListe(List<Integer> lista) {
        int LICZBA_OBROTOW = 500000;
        long poczatek, koniec;
        long suma;

        System.out.println("WSTAWIANIE NA KONIEC");

        poczatek = System.currentTimeMillis();
        for (int i = 0; i < LICZBA_OBROTOW; i++) {
            lista.add(i);
        }
        koniec = System.currentTimeMillis();
        System.out.println(String.format("Czas: %s ms\n", koniec - poczatek));

        //////////////

        System.out.println("WSTAWIANIE NA POCZĄTEK");

        poczatek = System.currentTimeMillis();
        for (int i = 0; i < LICZBA_OBROTOW; i++) {
            lista.add(0, i);
        }
        koniec = System.currentTimeMillis();
        System.out.println(String.format("Czas: %s ms\n", koniec - poczatek));

        //////////////


        System.out.println("\nWYBIERANIE PO INDEKSIE");
        poczatek = System.currentTimeMillis();
        suma = 0;
        for (int i = 0; i < LICZBA_OBROTOW; i += 100) {
            suma += lista.get(i);
            // działa szybko dla ArrayList, a wolno dla LinkedList
        }
        koniec = System.currentTimeMillis();
        System.out.println(String.format("Czas: %s ms", koniec - poczatek));
//        System.out.println(String.format("Suma: %s", suma));

        //////////////


        System.out.println("\nODCZYTANIE WSZYSTKICH ELEMENTÓW");
        poczatek = System.currentTimeMillis();
        suma = 0;
        for (int x : lista) {
            suma += x;
        }
        koniec = System.currentTimeMillis();

        System.out.println(String.format("Czas: %s ms", koniec - poczatek));
//        System.out.println(String.format("Suma: %s", suma));

    }

}
