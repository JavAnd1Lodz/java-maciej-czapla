package pliki;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class OdczytZPliku {

    public static void main(String[] arg) {
        File file = new File("tekst.txt");

        try {
            Scanner scanner = new Scanner(file);
            String zdanie = scanner.nextLine();
            System.out.println(zdanie);
        } catch (FileNotFoundException e) {
            System.out.print("Brak pliku!");
            e.printStackTrace();
        }

    }

}
