package sda.maciejczapla.konwencje;

// nazwy klas wielką literą
public class Konwencja {    // klamra na końcu linii

    int zmienna;
    String imie = "Maciej";
    static String STALA = "KLUCZ";
    int kwota = 1_000_000_000;


    void metoda() {
    }

    void drugaMetoda() {
    }

    private void trzeciaMetoda(String nazwa) {
    }

    protected void czwartaMetoda(String... parametry) {
//        parametry[0];
    }

    public void piataMetoda(String pierwszy, String... kolejne) {
        // kolejne[0]
    }

    public static void szostaMetoda() {
        // Klasa.szostaMetoda()
    }

}
