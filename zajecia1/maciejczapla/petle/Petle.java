package sda.maciejczapla.petle;

public class Petle {

    void petlaWhile() {
        int i = 1;
        while (i < 5) {
            System.out.println(i);
            i++;
        }
    }

    void doWhile() {
        int i = 1;
        do {
            System.out.println(i);
            i++;
        } while (i < 5);
    }

    void petlaFor() {
        for (int i = 0; i < 5; i++, System.out.println(i)) {
        }

        // petla nieskonczona
        for (; ; ) {

        }

//        for (int i = 5, j = 10; i < 5 && j < 3; j++, i++, System.out.print(i)) {
//
//        }

    }


    void forEach() {
        String[] imiona = {"Ala", "Ola", "Ela"};

        for (String imie : imiona) {

        }
    }


    void przerwania() {

        for (int i = 0; i < 10; i++) {

            if (i % 2 == 0) {
                continue;
            }
            System.out.println(i);

            if (i % 3 == 0) {
                break;
            }
        }
    }


    void etykiety() {
        int suma = 0;

        zewnetrzna:
        while (true) {
            wewnetrzna:
            for (int i = 0; i < 10; i++) {
                suma += i;
                if (suma > 50) {
                    break zewnetrzna;
                }
            }
        }

    }
}
