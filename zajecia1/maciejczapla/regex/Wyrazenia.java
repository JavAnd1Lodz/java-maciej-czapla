package regex;

public class Wyrazenia {

    static void sprawdz(String slowo, String wzorzec){
        System.out.println(String.format("\nSłowo: %s\nWzorzec: %s\nCzy pasuje: %s",
                slowo, wzorzec, slowo.matches(wzorzec) ? "Tak" : "Nie"));
    }

    public static void main(String[] args) {
    sprawdz("aabb", "aabb");

    sprawdz("aab", "a*b");
    sprawdz("ab", "a+b");

    sprawdz("b", "a?b");

    sprawdz("abbbba", "ab{1,4}a");

    sprawdz("a", "[abcde]");

    sprawdz("m", "[a-z]");

    sprawdz("m", "[a-g3-5]");

    sprawdz("abcdbcdbcd", "a(bcd)*");

    sprawdz("abcd", "a(b(cd)?)+");



    }
}
