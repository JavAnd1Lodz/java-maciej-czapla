package rekurencja;

public class Rekurencja {

    public int sumaIteracyjna(int wartosc) {
        int suma = 0;

        while (wartosc > 0) {
            suma += wartosc;
            wartosc--;
        }
        return suma;
    }

    public int sumaRekurencja(int wartosc) {
        if (wartosc > 0) {
            return wartosc + sumaRekurencja(wartosc - 1);
        } else {
            return 0;
        }
    }


}
