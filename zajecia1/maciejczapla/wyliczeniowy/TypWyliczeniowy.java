package wyliczeniowy;

public class TypWyliczeniowy {
    void metoda() {
        System.out.println(DniTygodnia.PONIEDZIALEK.czyUlubiony);
    }
}

enum DniTygodnia {
    PONIEDZIALEK(false), WTOREK(false), SOBOTA(true);

    boolean czyUlubiony;

    DniTygodnia(boolean ulubiony) {
        this.czyUlubiony = ulubiony;
    }
}