import rekurencja.Rekurencja;
import szybkosci.PorownanieSzybkosci;

import java.util.ArrayList;
import java.util.LinkedList;

public class Main {

    public static void main(String[] argumenty) {
//        PorownanieSzybkosci ps = new PorownanieSzybkosci();
//
//        LinkedList<Integer> linkedList = new LinkedList<>();
//        ArrayList<Integer> arrayList = new ArrayList<>();
//
//        System.out.println("LinkedList");
//        ps.testujListe(linkedList);
//
//        System.out.println("\n\n==========\n\n");
//
//        System.out.println("ArrayList");
//        ps.testujListe(arrayList);

        Rekurencja r = new Rekurencja();

        int max = 10000;
        System.out.println(r.sumaIteracyjna(max));
        System.out.println(r.sumaRekurencja(max));

    }
}
